//
//  ViewController.swift
//  ActionPrototype
//
//  Created by Anders Melen on 4/28/19.
//  Copyright © 2019 Anders Melen. All rights reserved.
//

import UIKit

class DelegateExample: UITableViewController, CompoundDelegate {

    let tableData: [CompoundDelegateComponent.ViewModel] = [
        .init(),
        .init(),
        .init()
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Delegate Example"
    }
    
    // MARK: - UITableViewDatasource / Delegate
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableData.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: nil)
        let component = CompoundDelegateComponent(frame: .zero)
        component.delegateComponent.textField.tag = indexPath.row
        component.actions = self
        cell.contentView.addSubview(component)
        component.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([component.leadingAnchor.constraint(equalTo: cell.contentView.safeAreaLayoutGuide.leadingAnchor),
                                     component.trailingAnchor.constraint(equalTo: cell.contentView.safeAreaLayoutGuide.trailingAnchor),
                                     component.topAnchor.constraint(equalTo: cell.contentView.safeAreaLayoutGuide.topAnchor),
                                     component.bottomAnchor.constraint(equalTo: cell.contentView.safeAreaLayoutGuide.bottomAnchor)])
        return cell
    }
    
    // MARK: - Actions
    // MARK: Actionable Actions
    func actionable(action: ActionableComponent.Action) {
        switch action {
        case .buttonPressed:
            print("VIEWCONTROLLER: buttonPressed")
        }
    }
    
    // MARK: Delegate Actions
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        print("VIEWCONTROLLER: ENTERED: \(string)")
        return true
    }
    
    // MARK: Selector
    func selectorButtonPressed() {
        print("VIEWCONTROLLER: BUTTON TAPPED")
    }
}
