//
//  ClosureExample.swift
//  ActionPrototype
//
//  Created by Anders Melen on 4/29/19.
//  Copyright © 2019 Anders Melen. All rights reserved.
//

import UIKit

class ClosureExample: UITableViewController {
    
    let tableData: [CompoundActionableComponent.ViewModel] = [
        .init(),
        .init(),
        .init()
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Closure Example"
    }
    
    // MARK: - UITableViewDatasource / Delegate
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableData.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: nil)
        let component = CompoundActionableComponent(frame: .zero)
        component.actions = { [weak self] action in
            guard let self = self else { return nil }
            return self.action(action: action, viewModel: self.tableData[indexPath.row])
        }
        cell.contentView.addSubview(component)
        component.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([component.leadingAnchor.constraint(equalTo: cell.contentView.safeAreaLayoutGuide.leadingAnchor),
                                     component.trailingAnchor.constraint(equalTo: cell.contentView.safeAreaLayoutGuide.trailingAnchor),
                                     component.topAnchor.constraint(equalTo: cell.contentView.safeAreaLayoutGuide.topAnchor),
                                     component.bottomAnchor.constraint(equalTo: cell.contentView.safeAreaLayoutGuide.bottomAnchor)])
        return cell
    }
    
    // MARK: - Actions
    func action(action: CompoundActionableComponent.Action, viewModel: CompoundActionableComponent.ViewModel) -> Any? {
        switch action {
        case .actionableComponent(let actionableAction): actionable(action: actionableAction, viewModel: viewModel)
        case .delegateComponent(let delegateAction): return delegate(action: delegateAction, viewModel: viewModel)
        case .selectorComponent(let selectorAction): return selector(action: selectorAction, viewModel: viewModel)
        }
        return nil
    }
    
    // MARK: Actionable Actions
    func actionable(action: ActionableComponent.Action, viewModel: CompoundActionableComponent.ViewModel) {
        switch action {
        case .buttonPressed:
            print("VIEWCONTROLLER: buttonPressed")
        }
    }
    
    // MARK: Delegate Actions
    func delegate(action: UITextFieldDelegateAction, viewModel: CompoundActionableComponent.ViewModel) -> Any? {
        print("VIEWCONTROLLER: DELEGATE \(action)")
        return true
    }
    
    // MARK: Selector
    func selector(action: CompoundActionableComponent.Action.SelectorComponentAction, viewModel: CompoundActionableComponent.ViewModel) {
        switch action {
        case .buttonPressed:
            print("VIEWCONTROLLER: SELECTOR buttonPressed")
        }
    }
    
}
