//
//  Actionable.swift
//  ActionPrototype
//
//  Created by Anders Melen on 4/28/19.
//  Copyright © 2019 Anders Melen. All rights reserved.
//

import Foundation

protocol Actionable {
    associatedtype Actions
    var actions: Actions? { get set }
}

protocol Component {
    associatedtype ViewModel
}
