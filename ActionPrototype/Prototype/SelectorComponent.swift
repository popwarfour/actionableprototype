//
//  SelectorComponent.swift
//  ActionPrototype
//
//  Created by Anders Melen on 4/29/19.
//  Copyright © 2019 Anders Melen. All rights reserved.
//

import UIKit

/// A subclass of a ui element that uses target/actions
class SelectorComponent: UIButton, Component {
    struct ViewModel { }
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit() {
        backgroundColor = .red
    }
}
