//
//  DelegateComponent.swift
//  ActionPrototype
//
//  Created by Anders Melen on 4/29/19.
//  Copyright © 2019 Anders Melen. All rights reserved.
//

import UIKit

/// A wrapper around a ui element that uses delegates
class DelegateComponent: UIView, Component, Actionable {
    typealias Actions = UITextFieldDelegate
    var actions: UITextFieldDelegate? {
        get {
            return textField.delegate
        }
        set {
            textField.delegate = newValue
        }
    }
    struct ViewModel { }
    
    let textField = UITextField(frame: .zero)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit() {
        backgroundColor = .green
        addSubview(textField)
        textField.placeholder = "ENTER TEXT"
        textField.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([textField.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor),
                                     textField.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor),
                                     textField.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor),
                                     textField.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor)])
    }
}
