//
//  CompoundActionableComponent.swift
//  ActionPrototype
//
//  Created by Anders Melen on 4/29/19.
//  Copyright © 2019 Anders Melen. All rights reserved.
//

import UIKit

/// This can be reused for other components that use the `UITextFieldDelegate` protocol as an action
enum UITextFieldDelegateAction {
    case textField(_ textField: UITextField, shouldChangeCharactersInRange: NSRange, replacementString: String)
    case textFieldShouldBeginEditing(_ textField: UITextField)
    case textFieldDidBeginEditing(_ textField: UITextField)
    case textFieldShouldEndEditing(_ textField: UITextField)
    case textFieldDidEndEditing(_ textField: UITextField)
    case textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason)
    case textFieldShouldClear(_ textField: UITextField)
    case textFieldShouldReturn(_ textField: UITextField)
}

/// A compound component with an actions interface implemented using `Actionable` closure.
class CompoundActionableComponent: UIView, Component, Actionable, UITextFieldDelegate {
    
    let stackview = UIStackView(frame: .zero)
    let actionableComponent = ActionableComponent()
    let delegateComponent = DelegateComponent()
    let selectorComponent = SelectorComponent()
    
    enum Action {
        enum SelectorComponentAction {
            case buttonPressed
        }
        case actionableComponent(ActionableComponent.Action)
        case delegateComponent(UITextFieldDelegateAction)
        case selectorComponent(SelectorComponentAction)
    }
    typealias Actions = (Action) -> Any?
    var actions: Actions?
    
    struct ViewModel { }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit() {
        backgroundColor = .orange
        // Stackview
        stackview.axis = .vertical
        stackview.distribution = .fillEqually
        addSubview(stackview)
        stackview.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([stackview.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor),
                                     stackview.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor),
                                     stackview.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor),
                                     stackview.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor)])
        // Actionable Component
        stackview.addArrangedSubview(actionableComponent)
        actionableComponent.actions = { [weak self] action in
            self?.action(action: action)
        }
        // Delegate Component
        stackview.addArrangedSubview(delegateComponent)
        delegateComponent.actions = self
        // Selector Component
        stackview.addArrangedSubview(selectorComponent)
        selectorComponent.addTarget(self, action: #selector(selectButtonPressed(_:)), for: .touchUpInside)
    }
    
    // MARK: - Actions
    /// A small wrapper that let's us easily ignore the return value. Would also be a good place to do other things like `DispatchQueue.main.async`, etc
    @discardableResult
    func sendAction(_ action: Action) -> Any? {
        return actions?(action)
    }
    
    // MARK: - Intercept Actions
    // MARK: Actionable
    let isAcceptingActionable = true
    func action(action: ActionableComponent.Action) {
        print("COMPOUND COMPONENT: ACTIONABLE: \(action)")
        // Intercept action and control the message flow
        guard isAcceptingActionable else { return }
        // Foward Action to Closure
        sendAction(.actionableComponent(action))
    }
    
    // MARK: Delegate
    let isAcceptingDelegate = true
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        print("COMPOUND COMPONENT: ENTERED: \(string)")
        // Intercept delegate and control the message flow
        guard isAcceptingDelegate else { return true }
        // Foward Delegate Method Invocation
        if let boolean = sendAction(.delegateComponent(.textField(textField, shouldChangeCharactersInRange: range, replacementString: string))) as? Bool {
            return boolean
        } else {
            return true
        }
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if let boolean = sendAction(.delegateComponent(.textFieldShouldBeginEditing(textField))) as? Bool {
            return boolean
        } else {
            return true
        }
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        sendAction(.delegateComponent(.textFieldDidBeginEditing(textField)))
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if let boolean = sendAction(.delegateComponent(.textFieldShouldEndEditing(textField))) as? Bool {
            return boolean
        } else {
            return true
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        sendAction(.delegateComponent(.textFieldDidEndEditing(textField)))
    }
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        sendAction(.delegateComponent(.textFieldDidEndEditing(textField, reason: reason)))
    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        if let boolean = sendAction(.delegateComponent(.textFieldShouldClear(textField))) as? Bool {
            return boolean
        } else {
            return true
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let boolean = sendAction(.delegateComponent(.textFieldShouldReturn(textField))) as? Bool {
            return boolean
        } else {
            return true
        }
    }
    
    // MARK: Selector
    let isAcceptingSelector = true
    @objc func selectButtonPressed(_ sender: UIButton) {
        print("COMPOUND COMPONENT: BUTTON TAPPED")
        // Intercept action and control the message flow
        guard isAcceptingSelector else { return }
        // Forward event
        sendAction(.selectorComponent(.buttonPressed))
    }
}

