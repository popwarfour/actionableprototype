//
//  CompoundComponent.swift
//  ActionPrototype
//
//  Created by Anders Melen on 4/29/19.
//  Copyright © 2019 Anders Melen. All rights reserved.
//

import UIKit

protocol CompoundDelegate: UITextFieldDelegate {
    func actionable(action: ActionableComponent.Action)
    func selectorButtonPressed()
}
extension CompoundDelegate {
    func actionable(action: ActionableComponent.Action) {
        // default, no-op
    }
    func selectorButtonPressed() {
        // default, no-op
    }
}

/// A compound component with an actions interface implemented using delegates.
class CompoundDelegateComponent: UIView, Component, Actionable, UITextFieldDelegate {
    
    let stackview = UIStackView(frame: .zero)
    let actionableComponent = ActionableComponent()
    let delegateComponent = DelegateComponent()
    let selectorComponent = SelectorComponent()
    
    typealias Actions = CompoundDelegate
    weak var actions: CompoundDelegate?
    struct ViewModel { }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit() {
        backgroundColor = .orange
        // Stackview
        stackview.axis = .vertical
        stackview.distribution = .fillEqually
        addSubview(stackview)
        stackview.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([stackview.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor),
                                     stackview.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor),
                                     stackview.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor),
                                     stackview.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor)])
        // Actionable Component
        stackview.addArrangedSubview(actionableComponent)
        actionableComponent.actions = { [weak self] action in
            self?.action(action: action)
        }
        // Delegate Component
        stackview.addArrangedSubview(delegateComponent)
        delegateComponent.actions = self
        // Selector Component
        stackview.addArrangedSubview(selectorComponent)
        selectorComponent.addTarget(self, action: #selector(selectButtonPressed(_:)), for: .touchUpInside)
    }
    
    // MARK: - Intercept Actions
    // MARK: Actionable
    let isAcceptingActionable = true
    func action(action: ActionableComponent.Action) {
        print("COMPOUND COMPONENT: ACTIONABLE: \(action)")
        // Intercept action and control the message flow
        guard isAcceptingActionable else { return }
        // Foward Action to Closure
        actions?.actionable(action: action)
    }
    
    // MARK: Delegate
    let isAcceptingDelegate = true
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        print("COMPOUND COMPONENT: ENTERED: \(string)")
        // Intercept delegate and control the message flow
        guard isAcceptingDelegate else { return true }
        // Foward Delegate Method Invocation
        return actions?.textField?(textField, shouldChangeCharactersIn: range, replacementString: string) ?? true
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return actions?.textFieldShouldBeginEditing?(textField) ?? true
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        actions?.textFieldDidBeginEditing?(textField)
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return actions?.textFieldShouldEndEditing?(textField) ?? true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        actions?.textFieldDidEndEditing?(textField)
    }
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        actions?.textFieldDidEndEditing?(textField, reason: reason)
    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        return actions?.textFieldShouldClear?(textField) ?? true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return actions?.textFieldShouldReturn?(textField) ?? true
    }
    
    // MARK: Selector
    let isAcceptingSelector = true
    @objc func selectButtonPressed(_ sender: UIButton) {
        print("COMPOUND COMPONENT: BUTTON TAPPED")
        // Intercept action and control the message flow
        guard isAcceptingSelector else { return }
        // Forward event
        actions?.selectorButtonPressed()
    }
}
