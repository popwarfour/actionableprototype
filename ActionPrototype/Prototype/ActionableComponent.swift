//
//  ActionableComponent.swift
//  ActionPrototype
//
//  Created by Anders Melen on 4/28/19.
//  Copyright © 2019 Anders Melen. All rights reserved.
//

import UIKit

/// A wrapper around a `UIButton` that uses a `Actionable` closure approach
class ActionableComponent: UIView, Component, Actionable {
    enum Action {
        case buttonPressed
    }
    typealias Actions = (Action) -> Void
    var actions: ActionableComponent.Actions?
    
    struct ViewModel { }
    
    let button = UIButton(frame: .zero)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit() {
        backgroundColor = .blue
        addSubview(button)
        button.addTarget(self, action: #selector(buttonPressed(_:)), for: .touchUpInside)
        button.setTitle("MY BUTTON", for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
   
        NSLayoutConstraint.activate([button.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor),
                                     button.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor),
                                     button.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor),
                                     button.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor)])
    }
    
    @objc func buttonPressed(_ sender: UIButton) {
        actions?(.buttonPressed)
    }
}


